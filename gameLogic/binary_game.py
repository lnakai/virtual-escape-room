#####################################################
#####################################################
##         barrels, BUCKETS, AND BINARY          ##
#####################################################
##                BY  LANNIN NAKAI                 ##
#####################################################
## Game where player has to figure out binary code ##
#####################################################
#####################################################

########################
## SCRIPT: GAME LOGIC ##
########################



###############################################
##            DETAILS: DEV NOTES             ##
###############################################

# The only door at the end of the room is locked - 1

# "40 C" is written on the wall in red paint - 2

# opposite that wall, written on a poster in blue paint is "fire and ice"
# "Some say the world will end in fire, some say in ice. From what I've tasted of desire I hold with those who
# favor fire. But if it had to perish twice, I think I know enough of hate To say that for destruction ice Is also
# great And would suffice" -3

# There are 8 barrels of increasing size lined up the room. if baralles are filled to 104 correctly door unlocks - 5

# There is a large pond of water in the corner of the room with bucket. But the water is frozen over the top - 9

# There is a shelf in the other corner of the room with 3 drawers, in the middle is a box of matches, the bottom
# shelf makes a strange creek when moved. If pulled out, the player can see writing on the bottom side of the shelf
# saying "match the value on the wall, using barrels small and tall, from right to left their value doubles, 
# starting from one and continuing past the earth in rubbles". - 7

# The smallest barrel is placed on a table. The table has a short leg, which is lifted by a book underneeth. 
# This book is book-marked on a page showing the f to c conversions - 5
# 25 C = 77 F
# 30 C = 86 F
# 33 C = 91.4 F
# 35 C = 95 F
# 40 C = 104 F
# 180 C = 356 F
# The player notices the book is very old and dry. The books pages can be torn out for kindeling, placed inside the
# wooden drawer. Take a nap and wake up to the unfronzen pond.

# there is a little scrap of paper with "knowledge is power, it can amaze or set the world ablaze" written on one side and a drawing alexander the great on the back - 4

# nothing - 6

# a table of equations showing "6 = _ + 2 + 4 + _ and 8 = _ + _ + _ + 8 and 3 = 1 + 2 + _ + _ " - 8

#############
## IMPORTS ##
#############

from map import Item, Area, Map
from player import Player

##############
##  PLAYER  ##
##############

player = Player(None, None, [])

###############
##   ITEMS   ##
###############

book = Item("book", ["The book has a book-mark inside of it", "You flip the book open to the book-marked page titled 'Celcius to ferenheight' with the below table: \n 25 C = 77 F \n 30 C = 86 F \n 33 C = 91.4 F \n 35 C = 95 F \n 40 C = 104 F \n 180 C = 356 F", "Flipping through the pages of the book, you realize how old it must be since the pages are very dry."], ["you tear out the pages of the book and place them in your pocket."],None, False)
matches = Item("box of matches", ["a box of twenty matches, they appear to be in good condition."], ["You fill the drawer you took off of the cabinet with the dry paper you tore from the book. Then, placing the drawer near the frozen pond you, you light a match from the match box and throw all the matches into the drawer. A strong blaze begins and the room is filled with warmth. Realizing it may take a while for the ice to melt, you cozy up into the bed you found yourself in take a nap"])
bucket = Item("bucket", ["A wooden bucket you found next to the frozen pond. It might be useful for something if only all the water you had access to wasn't frozen.",0]) # The second element in the detail list will indicate if the bucket is filled or not (0 = empty, 1 = full)
drawer = Item("drawer", ["A wooden drawer you were able to pull out of the shelf. The wood is very dried out, almost flakey."])
barrels = Item("barrels", [0,0,0,0,0,0,0,0],None, False) #all the barrels are empty to start (=0) and will be equal to 1 when filled
#############
##  Areas  ##
#############

Area1 = Area(["A corner of the room with a closed steel door. It is completely smooth... no handles or keyholes."], [], ["Try to open the door using the handle", "Kick it", "Knock", "Leave area"], 1)
Area1.responses = ["You try to turn the door handle but it doesn't budge... it's locked.", "You end up putting a bigger dent in your foot than in the door. Ouch.", "after waiting for several seconds, you accept your knock won't be returned."]

Area2 = Area(["'40 C' is written on the wall in red paint"],[],["Look closer", "Touch paint", "Leave area"],2)
Area2.responses = ["You lean in closer to the writing on the wall. There are little red flecks of something on the wall, you can't tell what though.", "You trace your finger along the letters. Yuck! The paint is slimey... it must have been written recently. The writing is on the wall, you have to get out of here!"]

Area3 = Area(["Written (in blue... paint?) is the following:\n Some say the world will end in fire, \n some say in ice.\n From what I've tasted of desire \n I hold with those who favor fire.\n But if it had to perish twice,\n I think I know enough of hate \n To say that for destruction ice \n Is also great \n And would suffice", "You smell the painting and it smells like... blueberries...", "Looking closer, you realize the word fire is lightly underlined"],[],["Taste the paint", "Read the poem aloud", "Leave area"],3)
Area3.responses = ["BLEH! That was as bad of an idea as you thought it would be. Definetly berry juice... also definetly not edible.", "You recite the classic poem aloud, seconds after you do, a faint applause is heard above you. You decide you need to figure out how to get out of this room before the Robert Frost fan decides to pay you a visit."]

Area4 = Area(["there is a little scrap of paper with 'knowledge is power, it can amaze or set the world ablaze' written on one side  of the paper and a drawing Alexander the Great suited in his armor riding into battle on his horse on the back of the paper", "Alexander's hair is painted firey red, looking like a fire with the wind blowing through it"],["Read aloud", "Leave area"], [],4)
Area4.responses = ["... silence... you aren't sure what you were expecting."]

Area5 = Area(["There are 8 barrels of increasing size lined up across the center of the room", "The smallest barrel is placed on a table. You notice the table has a short leg, which is lifted by a book underneeth."],[book], ["Look at barrels", "Look inside barrels", "Leave area"],5)
Area5.responses = ["From your right to left, the first barrel is very small, but each barrel seems to be twice the size of its' predecessor.", "They're all empty on the inside, reminds you of a coworker of yours."]
extra_options5 = ["Fill barrels", "Empty barrels","Take book"] #barrel filling logic taken care of in map.py
# when the first detail is popped off the option list, replace look at barrels with "Take book"
# when the pond has been melted and the bucket is equipped, the player can fill barrels

Area6 = Area(["Nothing to see here, just some dust on the floo -AHHHHHH!!! Nothing to see here, just some dust on the floor AND rats... a lot of rats.", "You wonder why you're even looking into this corner again..."],[],["Stay and look at rats", "Leave area like a sane person"],6)
Area6.responses = ["These are the strangest rats you've ever seen. A little rat with a chefs hat scurries from one hole in the wall to another, all with a cherry in its mouth. This makes you realize how hungry you are, and remember to hustle to get out of here!"]

Area7 = Area(["There is a cabinet in the other corner of the room with 3 drawers"], [matches, drawer],["Open top drawer","Open middle drawer", "Open bottom drawer","Leave area"] ,7)
Area7.responses = ["You open the top drawer and find a whole lot of ... nothing","You open the middle drawer, inside is a box of matches", "You open the bottom shelf and makes a strange creek when moved."]
extra_options7 = ["Take matches", "Take out bottom drawer"]
extras_responses7 = ["You decide to pull out the drawer, you can see writing on the bottom side of the shelf saying:\n 'match the value on the wall, \n using barrels small and tall, \n from right to left their value doubles, \n starting from one and continuing past the earth in rubbles'"]


Area8 = Area(["Written on the floor beside the wall, is a table of equations written in purple paint showing: \n 6 = 0 + 2 + 4 + 0 \n 8 = 0 + 0 + 0 + 8 \n 3 = 1 + 2 + 0 + 0 "], ["The fragrence of the paint is very strong, smell paint?", "Leave area"], 8)
Area8.responses = ["Yup, grape."]

Area9 = Area(["There is a large pond of water in the corner of the room with bucket. But the water is frozen over the top"],[bucket],["Try to crack ice","Take bucket","Leave area"], 9)
Area9.responses = ["Ouch! Yeah, the ice will not be what's breaking if you keep kicking.","bucket added to inventory"]

areas = ["filler",Area1,Area2,Area3,Area4,Area5,Area6,Area7,Area8,Area9] #list of areas

######################
## HELPER FUNCTIONS ##
######################

def game_introduction():
    # introductory steps of each game
    print("Darkness. You move your head to the side and hear rusteling. Standing up in a frightened state you step on the bedsheet you're wrapped in and fall to the floor. Where the hell are you is all you can think... until you realize you don't even know who you are... what was your name again?\n")
    name = input("name: ")
    player.name = name
    print("Alright {}, well after you removed the bedsheet from your head and made sure no one else saw what happened, you began to examine the room. The walls and floors of the room are made of cobblestone and are illuminated by ten small candles hanging several feet overhead. The candles don't light the room very well, so you have to move close to the area of the room you want to examine. You decide to begin exploring the room, hoping to find a way out of here... wherever 'here' is.\n".format(name))

def examine(area):
    # combines the two below functions
    examine_area(area)
    examine_options(area)

def examine_area(area):
    # displays appropriate information to player about the area they are examining
    print("You walk to section" + str(area.location) + "of the room and see " + area.details[0] + " \n What do you do? \n")
    if len(area.details) > 1:
        area.unlock()

def examine_options(area):
    # provides player options of what they can do in their current area
    
    i = 1 #incrementor for options display

    for option in area.options:
        print(str(i) + ") " + option + "\n") #list options for player
        i = i + 1

def area_select():
    # displays areas to Player and takes their input for which area to move to
    section = int(input("sections of room:\n \n |---|---|---| \n | 1 | 2 | 3 | \n |---|---|---| \n | 4 | 5 | 6 | \n |---|---|---| \n | 7 | 8 | 9 | \n |---|---|---| \n \n What section would you like to look at: \n"))
    player.location = section #adjust players location
    return section

def option_select():
    # takes user input for option choice, some special items will be unlocked by certain options
    return int(input("Select option by number: \n"))

def update():
    # check if any conditionals should be changed
    # Area5 conditionals (barrel/book) changing
    if len(Area5.details) == 1:
        Area5.options[0] = extra_options5[2]
        Area5.reponses[0] = "You now have a bucket. How exciting.\n"
    if player.location == 5 and (bucket in player.items):
        Area5.options.append(extra_options[0])
        Area5.responses.append("You filled the barrel \n")
        Area5.options.append(extra_options[1])
        Area5.responses.append("You emptied the barrel \n")
    
    

    
    


##################
##   GAMEPLAY   ## 
##################

if __name__ == "__main__":

    solved = False # stays False until player beats the game
    stay = True # inidcates whether or not player wants to stay in the area they are currently in
    melt = False # indicates if pond has been melted

    name = game_introduction() #generic game intro that gathers player's name

    action = area_select()
    current_area = areas[action]

    while not solved:
        if not stay:               # CASE 1: player leavees room
            action = area_select() # player selects next room
            current_area = areas[action] # current_area is changed
            examine_area(current_area)   # new area is examined
        update() # update options
        examine_options(current_area) # CASE 1 / 2: (re)examine area, get options
        action = option_select()      # player selects an action in the area
        if "Leave area" in current_area.options[action-1]:
            stay = False # CASE 1: leave room
        else:
            current_area.response(action) # CASE 2: get info about area
        if barrels.details == [0,1,1,0,1,0,0,0]: # check if room is solved
            solved = True
    # todo: victory sequence

#64 32 8 
#01101000
    