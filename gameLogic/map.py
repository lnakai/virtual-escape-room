
#####################################################
#####################################################
##         BARALLELS, BUCKETS, AND BINARY          ##
#####################################################
##                BY  LANNIN NAKAI                 ##
#####################################################
## Game where player has to figure out binary code ##
#####################################################
##################################################### 

#######################
## SCRIPT: MAP LOGIC ##
#######################

#making some "locations" classes on a "map" class for a generalized game building framework


class Item(): #items that can be held by player or placed in locations, functionality of items varies
    def __init__(self, name, details=[], use=None, holdable=True):
        self.name = name
        self.details = details
        self.use = use
        self.holdable = holdable

    def inspect(self): # output when player inspects an object
        print("Looking at {} : {}").format(self.name, self.details[0])
        print("\n")


    def unlock(self): # allows the player to access the next detail about the object (like discovering new features)
        details.pop(self.details[0])
    
    def use(self): # output when player uses item
        print(self.use)
        print("\n")

    def fill(self, index):
        # fill a barrel by changing a zero to a one in the barrels list
        self.details[index-1] = 1

class Area():
    def __init__(self, details=[], items = [], options = [], location = None, responses=[], roads=[]):
        # an area is just a spot on the map
        self.location = location # coordinates
        self.details = details
        self.options = options
        self.items = items
        self.discovered = False # whether or not player has been here
        self.roads = roads # where this area is linked to
        #if location != None:   for 2-d (or more complex) maps 
        #    self.size = max(location[0], location[1])
        self.location = location
        self.responses = responses

    def response(self, option): # returns the complimentary response when an option is chosen
        print("\n")
        print(self.responses[option-1])
        print("\n")

    def unlock(self): # allows the player to access the next detail about the object (like discovering new features)
        details.pop(self.details[0])

    
    def discover(self):
        # establishes contact with area
        self.discovered = True


class Map(): # organizes (usually by coordinates) and holds Area objects
    def __init__(self, size, areas=None):
        self.size = size
        if areas != []: # check if map is "filled out"
            self.areas = [area for area in areas if area.size < self.size]
        else:
            self.areas = areas

    def insert(self, area):
        #place a new area
        if area.size >= self.size: #make sure it fits on the map
            print("That area's coordinates aren't on this map")
            print("\n")
            return
        self.areas.append(area)
    
        
        
