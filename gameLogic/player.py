#####################################################
#####################################################
##         BARALLELS, BUCKETS, AND BINARY          ##
#####################################################
##                BY  LANNIN NAKAI                 ##
#####################################################
## Game where player has to figure out binary code ##
#####################################################
#####################################################

##########################
## SCRIPT: PLAYER LOGIC ##
##########################

#script for generalizable player functionality

#chekov's gun at the end

#############
## IMPORTS ##
#############
from map import Area

class Player():
    def __init__(self, name, location = None, items = []):
        #class for player, keeps track of their status
        self.name = name
        self.location = location
        self.items = items
    
    def move(self, area):
        #move to a new area
        self.location = area.location

    def pickup(self, item):
        #pick up single item
        self.items.append(item)
    
    def multi_pickup(self, items):
        for item in items:
            self.items.append(item)